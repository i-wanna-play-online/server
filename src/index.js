import { createServer } from "net";
import { createServer as _createServer } from "http";
import { createSocket } from "dgram";
import { SmartBuffer } from "smart-buffer";
import { PORT_HTTP, PORT_SOCKETS, PORT_SOCKETS_UDP } from "./env.js";
import { logger } from "./logger.js";

const LAST_VERSION = "1.2.0";
const MINUTES_AWAITING = 5;
const MINUTES_CHECKING = 2;
const MAX_PLAYERS_WITH_SAME_IP = 8;
const HEARTBEAT_DELAY_SECONDS = 10;
const players = [];
const playersUDP = [];
const socketUDP = createSocket("udp4");

const isVersionCompatible = function(version){
	try {
		const v1 = version.split(".").map(x => parseInt(x));
		const v2 = LAST_VERSION.split(".").map(x => parseInt(x));
		for(let i = 0; i < 3; ++i){
			if(v1[i] < v2[i])
				return false;
			if(v1[i] > v2[i])
				return true;
		}
		return true;
	}
	catch(e){
		return false;
	}
}

const removePlayer = function(id, playerList){
	for(let i = playerList.length-1; i >= 0; --i){
		if(playerList[i].id == id)
			playerList.splice(i, 1);
	}
}

SmartBuffer.prototype.readUIntV = function(){
	const buffer = this.internalBuffer;
	const firstByte = buffer.readUInt8(this.readOffset);
	const offsets = [0x000000, 0x000080, 0x004080, 0x204080];
	const usedBits = [1, 2, 3, 3];
	let length = 4;
	if(firstByte & 1)
		length = 1;
	else if(firstByte & 2)
		length = 2;
	else if(firstByte & 4)
		length = 3;
	const value = (buffer.readUIntLE(this.readOffset, length) >> usedBits[length-1])+offsets[length-1];
	this.readOffset += length;
	return value;
}

SmartBuffer.prototype.insertUIntV = function(value, offset){
	const offsets = [0x000000, 0x000080, 0x004080, 0x204080];
	const usedBits = [1, 2, 3, 3];
	const prefixBits = [1, 2, 4, 0];
	let length = 4;
	if(value < offsets[1])
		length = 1;
	else if(value < offsets[2])
		length = 2;
	else if(value < offsets[3])
		length = 3;
	const buffer = Buffer.alloc(length);
	value = ((value-offsets[length-1]) << usedBits[length-1]) | prefixBits[length-1];
	buffer.writeUIntLE(value, 0, length);
	this.insertBuffer(buffer, offset);
}

const checkUDPConnections = function(){
	const curTime = new Date().getTime();
	for(let i = playersUDP.length-1; i >= 0; --i){
		if(playersUDP[i].lastConnection+60000*MINUTES_AWAITING < curTime || playersUDP[i].killed)
			playersUDP.splice(i, 1);
	}
	setTimeout(checkUDPConnections, 60000*MINUTES_CHECKING);
}

socketUDP.on("error", err => {
	logg.log(`UDP socket error:\n${err.stack}`);
	socketUDP.close();
	process.exit(1);
});

socketUDP.on("message", (data, remote) => {
	if(data.length > 127)
		return;
	let alreadyExists = false;
	let curPlayer;
	for(const player of playersUDP){
		if(player.address == remote.address && player.port == remote.port){
			alreadyExists = true;
			curPlayer = player;
			break;
		}
	}
	if(alreadyExists){
		if(curPlayer.killed)
			return;
		curPlayer.messages++;
	}else{
		curPlayer = {
			address: remote.address,
			port: remote.port,
			id: null,
			game: null,
			room: null,
			pRoom: null,
			messages: 0,
			messageTime: new Date().getTime(),
			killed: false,
			kill: function(){
				this.killed = true;
				if(this.id !== null)
					removePlayer(this.id, players);
			}
		};
		playersUDP.push(curPlayer);
	}
	curPlayer.lastConnection = new Date().getTime();
	if(curPlayer.lastConnection-curPlayer.messageTime > 1000){
		if(curPlayer.messages > 100){
			curPlayer.kill();
		}else{
			curPlayer.messages = 0;
			curPlayer.messageTime = curPlayer.lastConnection;
		}
	}
	if(curPlayer.killed)
		return;
	const message = SmartBuffer.fromBuffer(data);
	switch(message.readUInt8()){
		case 0:
			// Initialize connection
			if(message.length > 1)
				curPlayer.kill();
			break;
		case 1:
			// Receive position
			if(message.length > 127){
				curPlayer.kill();
			}else{
				curPlayer.id = message.readStringNT();
				curPlayer.game = message.readStringNT();
				curPlayer.room = message.readUInt16LE();
				const buf = message.toBuffer();
				for(const player of playersUDP){
					if(player.id === null)
						continue;
					if(player.id == curPlayer.id)
						continue;
					if(player.game != curPlayer.game)
						continue;
					if(player.room != curPlayer.room && player.room != curPlayer.pRoom)
						continue;
					if(player.killed)
						continue;
					socketUDP.send(buf, player.port, player.address);
				}
				curPlayer.pRoom = curPlayer.room;
			}
			break;
		default:
			curPlayer.kill();
	}
	message.destroy();
});

socketUDP.on("listening", () => {
	logger.info(`Server sockets UDP running at port ${socketUDP.address().port}`);
	checkUDPConnections();
});

socketUDP.bind(PORT_SOCKETS_UDP);

const getGames = () => {
	const games = {};
	for(const player of players){
		if(player.gameName == "")
			continue;
		if(games[player.game] === undefined)
			games[player.game] = [];
		let index = -1;
		for(let i = 0, n = games[player.game].length; i < n; ++i){
			if(player.gameName == games[player.game][i].name){
				index = i;
				break;
			}
		}
		if(index < 0)
			games[player.game].push({name: player.gameName, frequency: 1, hasPassword: player.hasPassword});
		else
			games[player.game][index].frequency++;
	}
	const result = [];
	for(const key in games){
		const game = games[key];
		let numberOfPlayers = 0;
		let name = "";
		let frequency = 0;
		let hasPassword = game[0].hasPassword;
		for(const curGame of game){
			numberOfPlayers += curGame.frequency;
			if(frequency < curGame.frequency){
				name = curGame.name;
				frequency = curGame.frequency;
			}
		}
		result.push({
			players: numberOfPlayers,
			name: hasPassword ? "" : name,
			hasPassword: hasPassword
		});
	}
	return result;
}

_createServer((req, res) => {
	res.writeHead(200, {
		"Content-Type": "application/json"
	});
	res.write(JSON.stringify({games: getGames()}));
	res.end();
}).listen(PORT_HTTP, () => logger.info(`Server http running at port ${PORT_HTTP}`));

const checkHeartbeats = function(){
	const curTime = new Date().getTime();
	const playersToRemove = [];
	for(const player of players){
		if(player.lastHeartbeat+1000*HEARTBEAT_DELAY_SECONDS*2 < curTime)
			playersToRemove.push(player);
	}
	for(const player of playersToRemove)
		player.quit();
	setTimeout(checkHeartbeats, 1000*HEARTBEAT_DELAY_SECONDS);
}

createServer(socket => {
	const curDate = new Date().getTime();
	const player = {
		address: socket.remoteAddress,
		id: `${socket.remoteAddress}${socket.remotePort}`,
		socket: socket,
		name: "",
		game: "",
		gameName: "",
		hasPassword: false,
		messages: 0,
		messageTime: curDate,
		lastHeartbeat: curDate
	};
	player.sendMessage = message => {
		message.insertUIntV(message.length, 0);
		player.socket.write(message.toBuffer());
		message.destroy();
	}
	player.sendToEveryoneElse = message => {
		message.insertUIntV(message.length, 0);
		for(const otherPlayer of players){
			if(player.id == otherPlayer.id)
				continue;
			if(player.game != otherPlayer.game || player.game == "")
				continue;
			otherPlayer.socket.write(message.toBuffer());
		}
		message.destroy();
	}
	player.quit = () => {
		removePlayer(player.id, playersUDP);
		removePlayer(player.id, players);
		const message = new SmartBuffer();
		message.writeUInt8(1);
		message.writeStringNT(player.id);
		player.sendToEveryoneElse(message);
		player.socket.destroy();
	}
	players.push(player);
	if(players.filter(p => p.address == player.address).length > MAX_PLAYERS_WITH_SAME_IP){
		player.quit();
	}else{
		const message2 = new SmartBuffer();
		message2.writeUInt8(6);
		message2.writeStringNT(player.id);
		player.sendMessage(message2);
	}
	player.socket.on("data", buffer => {
		player.messages++;
		const curTime = new Date().getTime();
		if(curTime-player.messageTime > 1000){
			if(player.messages > 20){
				player.quit();
			}else{
				player.messages = 0;
				player.messageTime = curTime;
			}
		}
		player.lastHeartbeat = new Date().getTime();
		if(buffer.length > 2000){
			player.quit();
		}else{
			const data = SmartBuffer.fromBuffer(buffer);
			while(data.readOffset+1 < data.length){
				const length = data.readUIntV();
				if(length > 1000){
					player.quit();
					break;
				}else{
					const message = SmartBuffer.fromBuffer(data.readBuffer(length));
					player.onMessage(message);
					message.destroy();
				}
			}
			data.destroy();
		}
	});
	player.onMessage = message => {
		let message2;
		switch(message.readUInt8()){
			case 0:
				// CREATED
				if(message.length > 1){
					player.quit();
				}else{
					message2 = new SmartBuffer();
					message2.writeUInt8(0);
					message2.writeStringNT(player.id);
					message2.writeStringNT(player.name);
					player.sendToEveryoneElse(message2);
				}
				break;
			case 1:
				// DESTROYED
				if(message.length > 1){
					player.quit();
				}else{
					player.exists = false;
					message2 = new SmartBuffer();
					message2.writeUInt8(1);
					message2.writeStringNT(player.id);
					player.sendToEveryoneElse(message2);
				}
				break;
			case 2:
				// HEARTBEAT
				if(message.length > 1)
					player.quit();
				break;
			case 3:
				// NAME
				if(message.length > 1000){
					player.quit();
				}else{
					player.name = message.readStringNT();
					player.game = message.readStringNT();
					player.gameName = message.readStringNT();
					const version = message.readStringNT();
					player.hasPassword = message.readUInt8() == 1;
					if(!isVersionCompatible(version)){
						message2 = new SmartBuffer();
						message2.writeUInt8(2);
						message2.writeStringNT(LAST_VERSION);
						player.sendMessage(message2);
						setTimeout(player.quit, 1000);
					}
				}
				break;
			case 4:
				// CHAT MESSAGE
				if(message.length > 1000){
					player.quit();
				}else{
					message = message.readStringNT();
					message2 = new SmartBuffer();
					message2.writeUInt8(4);
					message2.writeStringNT(player.id);
					message2.writeStringNT(message);
					player.sendToEveryoneElse(message2);
				}
				break;
			case 5:
				// SAVE
				const MAX_SAVES_AT_ONCE = 5;
				if(message.length > 16 * MAX_SAVES_AT_ONCE){
					player.quit();
				}else{
					gravity = message.readUInt8();
					x = message.readInt32LE();
					y = message.readDoubleLE();
					room = message.readUInt16LE();
					message2 = new SmartBuffer();
					message2.writeUInt8(5);
					message2.writeUInt8(gravity);
					message2.writeStringNT(player.name);
					message2.writeInt32LE(x);
					message2.writeDoubleLE(y);
					message2.writeInt16LE(room);
					player.sendToEveryoneElse(message2);
				}
				break;
			default:
				player.quit();
		}
	}
	player.socket.on("close", player.quit);
	player.socket.on("timeout", player.quit);
	player.socket.on("error", player.quit);
	player.socket.on("end", player.quit);
}).listen(PORT_SOCKETS, () => {
	logger.info(`Server sockets running at port ${PORT_SOCKETS}`);
	checkHeartbeats();
});
