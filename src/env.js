import  dotenv from "dotenv";
dotenv.config();

export const PORT_HTTP = process.env.PORT_HTTP;
export const PORT_SOCKETS = process.env.PORT_SOCKETS;
export const PORT_SOCKETS_UDP = process.env.PORT_SOCKETS_UDP;
